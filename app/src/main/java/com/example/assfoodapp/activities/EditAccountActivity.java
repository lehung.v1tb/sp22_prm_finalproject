package com.example.assfoodapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.assfoodapp.MainActivity;
import com.example.assfoodapp.R;
import com.example.assfoodapp.db.DBHelper;

public class EditAccountActivity extends AppCompatActivity {
    Button btnSave;
    EditText etName, etPass, etAddress, etPhone;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        etName = findViewById(R.id.etName);
        etPass = findViewById(R.id.etPass);
        etAddress = findViewById(R.id.etAddress);
        etPhone = findViewById(R.id.etPhone);

        DB = new DBHelper(this);
        btnSave = findViewById(R.id.btnSave);

        Intent intent = getIntent();
        String user = intent.getStringExtra("user");
        String pass = intent.getStringExtra("pass");
        String address = intent.getStringExtra("address");
        String phone = intent.getStringExtra("phone");

        etName.setText(user);
        etAddress.setText(address);
        etPass.setText(pass);
        etPhone.setText(phone);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uUser = etName.getText().toString();
                String uPass = etPass.getText().toString();
                String uAddress = etAddress.getText().toString();
                String uPhone = etPhone.getText().toString();

                if (TextUtils.isEmpty(uUser)||TextUtils.isEmpty(uPass)|TextUtils.isEmpty(uAddress)||TextUtils.isEmpty(uPhone)){
                    Toast.makeText(EditAccountActivity.this,"All fields Required", Toast.LENGTH_SHORT).show();
                }else{
                    boolean saved = DB.upadteData(uUser,uPass,uAddress,uPhone);
                    if (saved == true){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("username", user);
                        Toast.makeText(EditAccountActivity.this,"Edit Successfully!", Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                    }else{
                        Toast.makeText(EditAccountActivity.this,"Edit failed!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}