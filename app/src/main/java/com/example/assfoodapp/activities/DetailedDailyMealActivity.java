package com.example.assfoodapp.activities;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assfoodapp.R;
import com.example.assfoodapp.adapters.DetailedDailyAdapter;
import com.example.assfoodapp.models.DetailedDailyMode;

import java.util.ArrayList;
import java.util.List;

public class DetailedDailyMealActivity extends AppCompatActivity {

    ImageView imageView;
    RecyclerView recyclerView;
    List<DetailedDailyMode>  detailedDailyModeList;
    DetailedDailyAdapter dailyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_daily_meal);

        String type = getIntent().getStringExtra("type");

        imageView = findViewById(R.id.detailed_img);
        recyclerView = findViewById(R.id.detailed_rec);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        detailedDailyModeList = new ArrayList<>();
        dailyAdapter = new DetailedDailyAdapter(detailedDailyModeList);
        recyclerView.setAdapter(dailyAdapter);

        if(type != null && type.equalsIgnoreCase("breakfast")) {
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.fav1, "Breakfast", "description", "4.4", "40","10am to 9pm"));
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.fav2, "Breakfast", "description", "4.4", "40","10am to 9pm"));
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.fav3, "Breakfast", "description", "4.4", "40","10am to 9pm"));
            dailyAdapter.notifyDataSetChanged();
        }

        if(type != null && type.equalsIgnoreCase("sweets")) {
            imageView.setImageResource(R.drawable.sweets);
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.s1, "Sweet", "description", "4.4", "40","10am to 9pm"));
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.s2, "Sweet", "description", "4.4", "40","10am to 9pm"));
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.s3, "Sweet", "description", "4.4", "40","10am to 9pm"));
            detailedDailyModeList.add(new DetailedDailyMode(R.drawable.s4, "Sweet", "description", "4.4", "40","10am to 9pm"));
            dailyAdapter.notifyDataSetChanged();
        }
    }
}