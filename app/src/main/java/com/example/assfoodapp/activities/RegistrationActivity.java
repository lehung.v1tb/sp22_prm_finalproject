package com.example.assfoodapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.assfoodapp.MainActivity;
import com.example.assfoodapp.R;
import com.example.assfoodapp.db.DBHelper;

public class RegistrationActivity extends AppCompatActivity {
    EditText etUserName, etPassword, etConfirmPassword;
    Button btnRegister;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnRegister = findViewById(R.id.btnSingin);
        DB = new DBHelper(this);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = etUserName.getText().toString();
                String pass = etPassword.getText().toString();
                String repass = etConfirmPassword.getText().toString();
                
                if (TextUtils.isEmpty(user)||TextUtils.isEmpty(pass)||TextUtils.isEmpty(repass)){
                    Toast.makeText(RegistrationActivity.this,"All fields Required", Toast.LENGTH_SHORT).show();
                }else {
                    if (pass.equals(repass)){
                        Boolean checkuser = DB.checkusername(user);
                        if(checkuser == false){
                            Boolean insert = DB.insertData(user, pass);
                            if(insert == true){
                                Toast.makeText(RegistrationActivity.this,"Registered Successfully!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("username",user);
                                startActivity(intent);
                            }else{
                                Toast.makeText(RegistrationActivity.this,"Registration failed!", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(RegistrationActivity.this,"Username is exists!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(RegistrationActivity.this,"Password are not matching!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void login(View view) {
        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
    }

}