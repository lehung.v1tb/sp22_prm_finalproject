package com.example.assfoodapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.assfoodapp.MainActivity;
import com.example.assfoodapp.R;
import com.example.assfoodapp.db.DBHelper;
import com.example.assfoodapp.models.Account;

public class DetailedAccountActivity extends AppCompatActivity {
    Button btnEdit;
    EditText etName, etPass, etAddress, etPhone;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_account);
        btnEdit = findViewById(R.id.btnEdit);
        etName = findViewById(R.id.etName);
        etPass = findViewById(R.id.etPass);
        etAddress = findViewById(R.id.etAddress);
        etPhone = findViewById(R.id.etPhone);
        DB = new DBHelper(this);

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");

        Account account = DB.getAccount(username);
        etName.setText(account.getUsername());
        etAddress.setText(account.getAddress());
        etPass.setText(account.getPassword());
        etPhone.setText(account.getPhone());
        System.out.println(account.toString());
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = etName.getText().toString();
                String pass = etPass.getText().toString();
                String address = etAddress.getText().toString();
                String phone = etPhone.getText().toString();

                Intent intent = new Intent(getApplicationContext(), EditAccountActivity.class);
                intent.putExtra("user",user);
                intent.putExtra("pass",pass);
                intent.putExtra("address",address);
                intent.putExtra("phone",phone);
                startActivity(intent);
            }
        });
    }
}