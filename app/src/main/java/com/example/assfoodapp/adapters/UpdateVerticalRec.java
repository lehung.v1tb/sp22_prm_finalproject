package com.example.assfoodapp.adapters;

import com.example.assfoodapp.models.HomeVerModel;

import java.util.ArrayList;

public interface UpdateVerticalRec {
    public void callBack(int position, ArrayList<HomeVerModel> list);
}
