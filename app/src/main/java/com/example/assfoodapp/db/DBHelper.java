package com.example.assfoodapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.assfoodapp.models.Account;

public class DBHelper extends SQLiteOpenHelper {

    public static String DBNAME = "AppFood.db";
    public DBHelper( Context context) {
        super(context, "AppFood.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table users(username TEXT primary key, password TEXT, address TEXT, phone TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists users");
    }

    public Boolean insertData(String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username", username);
        values.put("password", password);
        long result = db.insert("users", null, values);
        if (result ==-1){
            return  false;
        }else{
            return  true;
        }
    }
    public Boolean upadteData(String username, String password, String address, String phone){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("username", username);
        values.put("password", password);
        values.put("address", address);
        values.put("phone", phone);

        long result = db.update("users",values,"username =?",new String[]{username} );
        if (result ==-1){
            return  false;
        }else{
            return  true;
        }
    }

    public Account getAccount(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query("users",new String[]{"username","password","address","phone"}, "username =?",
                new String[]{username},null,null,null,null);
        if (cursor != null)
            cursor.moveToFirst();
        Account account = new Account(cursor.getString(0), cursor.getString(1),cursor.getString(2), cursor.getString(3));
        return account;
    }

    public Boolean checkusername(String username){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from users where username = ?",new String[]{username});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return  false;
        }
    }

    public Boolean checkusernamepassword(String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from users where username = ? and password = ?",new String[]{username, password});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return  false;
        }
    }
}
